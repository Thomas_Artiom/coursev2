from rest_framework.routers import DefaultRouter

from main.views import *

router = DefaultRouter()

router.register('clients', ClientViewSet, 'clients')
router.register('categories', CategoryViewSet, 'categories')
router.register('cities', CityViewSet, 'cities')
router.register('tours', TourViewSet, 'tours')
router.register('trips', TripViewSet, 'trips')
router.register('photos', PhotoViewSet, 'photos')
router.register('reviews', ReviewViewSet, 'reviews')

urlpatterns = router.urls

from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from main.models import *


class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'full_name', 'phone', 'email', 'created_at')
    list_display_links = ('id', 'full_name', 'phone')
    list_filter = ('created_at',)
    search_fields = ('full_name', 'email')


class TourAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'city', 'price', 'created_at')
    list_display_links = ('id', 'title', 'city',)
    list_filter = ('city', 'created_at')
    search_fields = ('title', 'description')
    autocomplete_fields = ('category', 'city')


class TripAdmin(admin.ModelAdmin):
    list_display = ('id', 'client', 'tour', 'price', 'status', 'visited_at')
    list_display_links = ('id', 'client', 'tour')
    list_filter = ('created_at', 'visited_at')
    search_fields = ('client', 'tour')
    autocomplete_fields = ('client', 'tour')


class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'country', 'weight')
    list_display_links = ('id', 'title')
    list_filter = ('weight',)
    search_fields = ('title', 'country', 'category')
    autocomplete_fields = ('category',)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type')
    list_display_links = ('id', 'name', 'type')
    list_filter = ('type',)
    search_fields = ('name',)


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'image_', 'description')
    list_display_links = ('id', 'title')
    list_filter = ('category',)
    search_fields = ('title', 'description', 'category')
    autocomplete_fields = ('category',)

    def image_(self, obj):
        url = reverse('admin:main_photo_change', args=(obj.id,))
        return mark_safe(f'<a href="{url}"><img height="150px" src="/media/{obj.image}" /></a>')

    image_.short_description = 'Изображение'


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'trip', 'rating', 'created_at')
    list_display_links = ('id', 'author', 'trip')
    list_filter = ('created_at', 'rating')
    search_fields = ('description', 'author', 'trip')
    autocomplete_fields = ('author', 'trip')


admin.site.register(Tour, TourAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Trip, TripAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Review, ReviewAdmin)

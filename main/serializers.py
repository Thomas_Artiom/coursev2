from django.contrib.auth.models import User
from rest_framework import serializers

from main.models import *


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True)

    class Meta:
        model = City
        fields = '__all__'


class TourSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    category = CategorySerializer(many=True)

    class Meta:
        model = Tour
        fields = '__all__'


class TripSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    tour = TourSerializer()

    class Meta:
        model = Trip
        fields = '__all__'


class PhotoSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Photo
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    author = ClientSerializer()
    trip = TripSerializer()

    class Meta:
        model = Review
        fields = '__all__'

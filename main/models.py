from django.db import models


class Client(models.Model):
    first_name = models.CharField('Имя', max_length=32)
    second_name = models.CharField('Фамилия', max_length=32)
    last_name = models.CharField('Отчество', max_length=32)
    phone = models.CharField('Телефон', max_length=11)
    email = models.EmailField('Email', unique=True)
    created_at = models.DateTimeField('Дата присоединения', auto_now_add=True)

    @property
    def full_name(self):
        return f'{self.first_name} {self.second_name}'
    full_name.fget.short_description = 'Фамилия и Имя'

    def __str__(self):
        return f'{self.first_name} {self.second_name} ({self.phone})'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Tour(models.Model):
    title = models.CharField('Название', max_length=32)
    price = models.FloatField('Цена')
    city = models.ForeignKey('City', on_delete=models.SET_NULL, null=True, verbose_name='Город')
    category = models.ManyToManyField('Category', verbose_name='Категории')
    description = models.TextField('Описание')
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self):
        return f'{self.title} ({self.city.title})'

    class Meta:
        verbose_name = 'Тур'
        verbose_name_plural = 'Туры'


class Trip(models.Model):
    FINISH = 'finish'
    ON_TRIP = 'on_trip'
    WAITING = 'waiting'
    REGISTRATION = 'registration'

    STATUSES = [
        (FINISH, 'Завершено'),
        (ON_TRIP, 'В поездке'),
        (WAITING, 'Ожидание'),
        (REGISTRATION, 'Оформление')
    ]

    client = models.ForeignKey('Client', on_delete=models.SET_NULL, null=True, verbose_name='Клиент')
    tour = models.ForeignKey('Tour', on_delete=models.SET_NULL, null=True, verbose_name='Тур')
    count = models.IntegerField('Количество человек')
    price = models.FloatField('Итоговая цена')
    status = models.CharField('Статус', max_length=12, choices=STATUSES, default=REGISTRATION)
    visited_at = models.DateTimeField('Дата поездки')
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Поездка'
        verbose_name_plural = 'Поездки'


class City(models.Model):
    title = models.CharField('Название', max_length=64)
    country = models.CharField('Страна', max_length=64)
    weight = models.IntegerField('Значимость')
    category = models.ManyToManyField('Category', verbose_name='Категории')

    def __str__(self):
        return f'{self.title} ({self.country}, {self.weight})'

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Category(models.Model):
    CITY = 'city'
    TOUR = 'tour'
    PHOTO = 'img'

    CATEGORY_TYPES = [
        (CITY, 'Город'),
        (TOUR, 'Тур'),
        (PHOTO, 'Фотография')
    ]

    name = models.CharField('Название', max_length=32)
    type = models.CharField('Тип категории', max_length=4, choices=CATEGORY_TYPES, default=TOUR)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Photo(models.Model):
    image = models.ImageField('Изображение')
    title = models.CharField('Заголовок', max_length=32)
    description = models.CharField('Описание', max_length=256, blank=True, null=True)
    category = models.ManyToManyField('Category', verbose_name='Категории')

    def __str__(self):
        return f'Фотография "{self.title}"'

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'


class Review(models.Model):
    GREAT = 5
    COOL = 4
    NOT_BAD = 3
    BAD = 2

    RATING = [
        (GREAT, 'Отлично'),
        (COOL, 'Хорошо'),
        (NOT_BAD, 'Могло быть и лучше'),
        (BAD, 'Плохо')
    ]

    author = models.ForeignKey('Client', on_delete=models.CASCADE, verbose_name='Клиент')
    description = models.TextField('Описание')
    rating = models.IntegerField('Оценка', choices=RATING, default=GREAT)
    trip = models.ForeignKey('Trip', on_delete=models.SET_NULL, null=True, verbose_name='Поездка')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата оставления отзыва')

    def __str__(self):
        return f'Отзыв №{self.pk} от {self.trip.visited_at} ({self.author.full_name})'

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

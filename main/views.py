from datetime import datetime

from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from main.serializers import *


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAdminUser,)
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['full_name', 'email', 'phone']
    ordering_fields = ['created_at']

    @action(methods=['get'], detail=True)
    def history(self, request, pk=None):
        trips = Trip.objects.filter(client__id=pk)
        serializer = TripSerializer(trips, many=True)
        return Response(serializer.data)


class TourViewSet(viewsets.ModelViewSet):
    queryset = Tour.objects.all()
    serializer_class = TourSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['title', 'city', 'description']
    ordering_fields = ['created_at', 'price']
    filter_fields = ['category', 'city']


class TripViewSet(viewsets.ModelViewSet):
    queryset = Trip.objects.all()
    serializer_class = TripSerializer
    permission_classes = (IsAdminUser,)
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['client', 'tour']
    ordering_fields = ['created_at', 'visited_at', 'price', 'count']
    filter_fields = ['tour', 'count', 'status']

    @action(methods=['post'], detail=True)
    def rate(self, request, pk=None):
        if not Trip.objects.filter(pk=pk).exists():
            raise serializers.ValidationError('Такой поездки не существует.')

        trip = Trip.objects.get(pk=pk)
        rating = request.POST['rating']
        description = request.POST['description']

        if not (5 >= rating >= 1):
            raise serializers.ValidationError('Оценка поездки измеряется шкалой от 1 до 5.')
        if not description:
            raise serializers.ValidationError('Description является обззательным полем.')

        review = Review.objects.creae(
            author=trip.client,
            description=description,
            rating=rating,
            trip=trip
        )
        serializer = ReviewSerializer(review)
        return serializer.data

    @action(methods=['post'], detail=False)
    def in_progress(self, request):
        client = request.POST['client']

        trips = Trip.objects.filter(
            Q(client__first_name__icontains=client) &
            Q(visited_at__gt=datetime.now) &
            Q(status=Trip.WAITING)
        )

        serializer = TripSerializer(trips, many=True)
        return serializer.data


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['title', 'country']
    ordering_fields = ['weight']
    filter_fields = ['weight', 'category']


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['name']
    ordering_fields = ['type']
    filter_fields = ['type']


class PhotoViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['title', 'description', 'category']
    filter_fields = ['category']


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['description']
    ordering_fields = ['created_at', 'rating']
    filter_fields = ['rating', 'trip']
